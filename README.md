# python_web

#### 介绍
我啥也不说

#### 软件架构
软件架构说明
.
├── manage.py
├── myadmin
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   │   ├── __init__.py
│   │   └── __pycache__
│   │       └── __init__.cpython-35.pyc
│   ├── models.py
│   ├── __pycache__
│   │   ├── admin.cpython-35.pyc
│   │   ├── __init__.cpython-35.pyc
│   │   ├── models.cpython-35.pyc
│   │   ├── urls.cpython-35.pyc
│   │   └── views.cpython-35.pyc
│   ├── tests.py
│   ├── urls.py
│   └── views.py
├── myhome
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   │   ├── __init__.py
│   │   └── __pycache__
│   │       └── __init__.cpython-35.pyc
│   ├── models.py
│   ├── __pycache__
│   │   ├── admin.cpython-35.pyc
│   │   ├── __init__.cpython-35.pyc
│   │   ├── models.cpython-35.pyc
│   │   ├── urls.cpython-35.pyc
│   │   └── views.cpython-35.pyc
│   ├── tests.py
│   ├── urls.py
│   └── views.py
├── readme
├── README.en.md
├── README.md
├── static
├── templates
└── web
    ├── __init__.py
    ├── __pycache__
    │   ├── __init__.cpython-35.pyc
    │   ├── settings.cpython-35.pyc
    │   ├── urls.cpython-35.pyc
    │   └── wsgi.cpython-35.pyc
    ├── settings.py
    ├── urls.py
    └── wsgi.py



#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 更新说明
添加商品模型

1 class Goods(models.Model):
    # id 所属分类,商品名,图片,添加时间,销量
    cateid = models.ForeignKey(to="Cates", to_field="id")  #
    goodsname = models.CharField(max_length=50)
    title = models.CharField(max_length=255)
    price = models.FloatField()
    goodsnum = models.IntegerField()
    pic_url = models.CharField(max_length=255)
    goodinfo = models.TextField()
    ordernum = models.IntegerField(default=0)
    clickNum = models.IntegerField(default=0)
    # 0 新品 1 热卖  2 推荐  3 下架
    status = models.IntegerField(default=0)
    addtime = models.DateTimeField(auto_now_add=True)

2 添加了副文本编辑器



3  增加了动态获取图片的特效



